import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RoadCamera
{
    public static void main(String[] args) throws IOException
    {
        //Параметры программы
        int maxOncomingSpeed = 40; // km/h  скорость меньше 60 - штрафа нет
        int speedFineGrade = 20; // km/h
        int finePerGrade = 1000; // RUB   увеличил тут штраф в 2 раза
        int criminalSpeed = 180; // km/h

        //=============================================================

        System.out.println("Введите скорость автомобиля:");

        //Скорость автомобиля
        int oncomingSpeed = Integer.parseInt((new BufferedReader(new InputStreamReader(System.in))).readLine());
        if(oncomingSpeed >= criminalSpeed)
        {
            System.out.println("Вызов полиции...");
        }
        else if(oncomingSpeed > maxOncomingSpeed)
        {
            int overSpeed = oncomingSpeed - maxOncomingSpeed;
            int gradesCount = overSpeed / speedFineGrade;
            int fine = finePerGrade * gradesCount; // можно увеличить тут в 2 раза штраф ("finePerGrade * gradesCount * 2")
            System.out.println("Сумма штрафа: " + fine); // а можно тут (fine*2)
        }
        else {
            System.out.println("Скорость не превышена");
        }
    }
}